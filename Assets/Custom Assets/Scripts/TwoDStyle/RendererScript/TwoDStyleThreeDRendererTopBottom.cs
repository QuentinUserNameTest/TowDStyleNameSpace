﻿using UnityEngine;
using System.Collections;
namespace TwoDStyle
{
    public abstract class TwoDStyleThreeDRendererTopBottom : TwoDStyleThreeDRenderer
    {

        public float topViewAngle = 5.0f;

        /// <summary>
        /// the Top plate of any TwoDStyleRenderer
        /// </summary>
        [SerializeField]
        protected Transform topRender;
        /// <summary>
        /// the Top plate of any TwoDStyleRenderer
        /// </summary>
        public Transform TopRender
        {
            get
            {
                return topRender;
            }
            set
            {
                topRender = value;
            }
        }
        /// <summary>
        /// the bottom plate of any TwoDStyleRenderer
        /// </summary>
        [SerializeField]
        protected Transform bottomRender;
        /// <summary>
        /// the bottom plate of any TwoDStyleRenderer
        /// </summary>
        public Transform BottomRender
        {
            get
            {
                return bottomRender;
            }
            set
            {
                bottomRender = value;
            }
        }


        protected override void activePlate(Transform plate)
        {
            if (plate != activeRender)
            {
                if (sideRender != plate) { sideRender.gameObject.SetActive(false); }
                if (topRender != plate) { topRender.gameObject.SetActive(false); }
                if (bottomRender != plate) { bottomRender.gameObject.SetActive(false); }
                activeRender = plate;
                activeRender.gameObject.SetActive(true);
                activeRenderer = activeRender.GetChild(0).GetComponent<Renderer>();
            }
        }
        public override void FixTheCamera(Transform cam)
        {
            /*base.FixTheCamera(cam);*/
            orentationChecker.LookAt(cam);
            if (orentationChecker.localEulerAngles.x > 90.0f - topViewAngle && orentationChecker.localEulerAngles.x < 90.0f + topViewAngle)
            {
                activePlate(bottomRender);
            }
            else if (orentationChecker.localEulerAngles.x > 270.0f - topViewAngle && orentationChecker.localEulerAngles.x < 270.0f + topViewAngle)
            {
                activePlate(topRender);
            }
            else
            {
                activePlate(sideRender);
                faceToShow(orentationChecker.localEulerAngles.y);
                activeRender.localEulerAngles = new Vector3(0.0f, orentationChecker.localEulerAngles.y, 0.0f);
                //faceToShow(orentationChecker.localEulerAngles.y);
            }
        }
    }
}