﻿using UnityEngine;
using System.Collections;
namespace TwoDStyle
{
    public abstract class TwoDStyleThreeDRendererSideOnly : TwoDStyleThreeDRenderer
    {

        protected /*override*/ void Start()
        {
            /*base.Start();*/
            activePlate(sideRender);
            RenderingState = "pizza";
            Invoke("changeState", 2.0f);
        }
        private void changeState()
        {
            //Debug.Log(gameObject.name);
            RenderingState = "WOHO";
        }
        public override void FixTheCamera(Transform cam)
        {
            /*base.FixTheCamera(cam);*/
            orentationChecker.LookAt(cam);
            faceToShow(orentationChecker.localEulerAngles.y);
            activeRender.localEulerAngles = new Vector3(0.0f, orentationChecker.localEulerAngles.y, 0.0f);
            faceToShow(orentationChecker.localEulerAngles.y);
        }

        // Update is called once per frame
        /*
        protected /*override void Update()
        {
            //base.Update();
        }
        */
    }
}
